## Notion de base #

Il existe 3 bases :
- base 10 (decimale) Dans la vie de tous les jours, on compte en base 10, c'est-à-dire que nos nombres comportent 10 chiffres allant de 0 à 9.
- base 2 (binaire) Les nombres écrits en binaire se composent en fait des chiffres 0 et 1. Comme pour un interrupteur pour ampoule ( on et off).
- base 16 (hexadecimal) Les nombres écrits en hexadécimal se composent des chiffres 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A (=10), B (=11), C (=12), D (=13), E (=14) et F (=15).

**Calcul base n vers base decimal** <br />
chiffre a calculer × base n
Exemples : <br />
Décimal (base 10) : 1647  = 1×10^3+6×10^2+4×10^1+7×10^0 = 1647  (on retrouve bien le bon résultat) <br />
Binaire (base 2) : 101101 = 1×2^5+0×2^4+1×2^3+1×2^2+0×2^1+1×2^0 = 45 <br />
Hexadécimal (base 16) : 5C8 = 5×16^2+12×16^1+8×16^0 = 1280+192+8 = 1480 <br />

**Calcul base decimale vers autre base** <br />
Pour cela, on effectue des divisions euclidiennes successives par la base voulue : le premier reste va donner le chiffre le plus à droite, le deuxième reste l'avant-dernier chiffre etc. jusqu'à ce qu'on trouve un résultat nul. <br />
En notation hexadécimale, un chiffre donne 16 possibilités. De plus, 4 bits donnent également 16 possibilités. Pour alléger les notations, on peut donc noter chaque quartet par un chiffre hexadécimal. Chaque octet peut donc se noter par 2 chiffres hexadécimaux. Correspondance décimal <-> hexadécimal <-> binaire entre 0 et 15 :

![ALT](tableau1.png)


## Les Registres #

- **AX** : l'accumulateur (Accumulator) ; c'est le registre le plus utilisé car dessus sont concentrés de nombreux types de calculs possibles à effectuer... <br />
- **BX** : le registre de Base. Il est utilisé comme référence d'accès à des tableaux. <br />
- **CX** : le registre de Compteur. Il est utilisé pour les répétitions d'instructions et décalages logiques. <br />
- **DX** : le registre de Données. Il sert en fait d'extension à AX. <br />

![ALT](registre16b.png) <br />
_(les petits carrés bleus représentent des bits)_

Ici, il s'agit du cas de AX, mais cela revient au même avec BX, CX et DX. <br />
On a ainsi les registres 8 bits : AH, AL, BH, BL, CH, CL, DH et DL. <br />

Un registre de 1 octet, soit 8 bits, peut correspondre à 2^8 = 256 combinaisons différentes : pour ceux qui ne connaissent pas cette formule, un bit permet de représenter 2 combinaisons ; 2 bits, 2 fois plus etc. : on multiplie le nombre de possibilités par deux à chaque ajout d'un bit. Avec 8 bits, on a donc 2*2*2*2*2*2*2*2 = 2^8 = 256 combinaisons.

Un registre de 2 octets peut, quant à lui, valoir 2^16 = 65536 choses différentes. On peut donc stocker des nombres entiers :
- allant de 0 à 255 ou de -128 à 127, pour les registres 8 bits ;
- allant de 0 à 65535 ou de -32768 à 32767, pour les registres 16 bits.
Tout cela est une convention, on aurait pu par exemple choisir les nombres de -15 à 240, ou de 30 à 285... Ce qui est important, c'est le nombre de combinaisons. Leur signification est arbitraire ; les programmeurs la choisissent.

![ALT](combinaison8b.png)

Il y a donc deux types de nombres binaires selon la convention attribuée :
- les nombres considérés comme toujours positifs sont dits non signés (0 à 255 ou 0 à 65535) ;
- les nombres considérés comme pouvant être positifs ou négatifs sont dits signés (-128 à 127 ou -32768 à 32767). <br />
Voici un schéma pour les valeurs de 1 octet, pour mieux voir la façon dont on assigne une valeur à des bits :

![ALT](valeurs1octet.png)

Nous avons donc appris qu'il y avait des registres à 8 et 16 bits (respectivement 1 et 2 octets). Mais depuis longtemps, les ordinateurs sont passés au 32 bits (et même au 64). Nous disposons ainsi de registres 32 bits. <br />
Ce sont simplement des Extensions des registres généraux 16 bits : AX, BX, CX et DX. Les registres 32 bits correspondants se nomment EAX, EBX, ECX et EDX, avec "E" comme "Étendu" ("Extended", en fait) :  ce sont des registres qui étendent AX, BX, CX et DX. Leur partie haute n'est pas directement accessible de manière isolée, et leur partie basse correspond aux registres 16 bits :

![ALT](registre32b.png)


## Les données #

#### Les nombres #

Pour insérer un octet de donnée, on utilise la commande "db" (Define Byte) ; pour un mot (= 2 octets), on utilise "dw" (Define Word) ; et enfin pour un double-mot (= 4 octets), on utilise "dd" (Define Double word). Puis on fait suivre cela d'un nombre. Mais il ne doit pas dépasser 255 pour un octet, 65 535 pour un mot, et 4 294 967 295 pour un double-mot.
Nous avons vu comment définir un nombre en le donnant sous forme décimale. Mais on peut aussi l'exprimer en différentes bases :
- binaire : on fait suivre le nombre d'un "b" comme binaire ;
- décimal : on écrit tout simplement le nombre, et on peut aussi le faire suivre d'un "d" comme décimal (facultatif) ;
- hexadécimal : on fait suivre le nombre d'un "h" comme hexadécimal ou bien on le fait précéder de "0x".
Attention, une exception pour les nombres hexadécimaux : un nombre ne commence jamais par une lettre. Donc si vous utilisez la convention avec le suffixe "h" et que le nombre commence par un chiffre entre A et F, il faut ajouter un 0.

### Le texte #

Avec NASM, la norme automatiquement utilisée est l'ASCII : c'est une norme qui à chaque nombre 8 bits (1 octet) attribue un caractère. Cela permet de représenter jusqu'à 256 caractères. On peut ainsi enregistrer 1 caractère dans un registre 8 bits et deux caractères dans un registre 16 bits. Il suffit de mettre ces caractères entre guillemets ou apostrophes.

```
db "a"
db "A", 'h'
mov AX, "("
mov BH, 'U'
```

### Les etiquettes #

Pour repérer un endroit du programme, on utilise une étiquette. C'est un mot-clé placé en début de ligne et suivi des deux-points. Ce mot-clé ne doit pas être le même que celui d'une commande (mov, db, dw, org etc.), et ne peut être constitué que de caractères alphanumériques et du tiret-bas "_". <br />
Lorsqu'une étiquette est définie, on peut l'utiliser n'importe où dans le programme. Elle sera remplacée par la valeur qu'elle désigne lors de l'assemblage. On peut donc effectuer des opérations dessus, car c'est une valeur immédiate (valeur calculable dès de l'assemblage sans attendre l'exécution). <br />
*En revanche, il n'est pas possible de faire **mov etiquette, registre** ; cela n'aurait aucun sens car etiquette est une constante.*

### Compiler et lier un programme d'assemblage dans NASM #

Exemple avec Hello World !!

Assurez-vous que vous avez défini le chemin des fichiers binaires nasm et ld dans votre variable d’environnement PATH. Maintenant, suivez les étapes suivantes pour compiler et lier le programme ci-dessus -
- Tapez le code à l'aide d'un éditeur de texte et enregistrez-le sous le nom hello.asm.
- Assurez-vous que vous vous trouvez dans le même répertoire que celui où vous avez enregistré hello.asm .
- Pour assembler le programme, tapez nasm -f elf (ou bin) hello.asm
S'il y a une erreur, vous en serez informé à ce stade. Sinon, un fichier objet de votre programme nommé hello.o sera créé.
- Pour lier le fichier objet et créer un fichier exécutable nommé hello, tapez ld -m elf_i386 -s -o hello hello.o
- Exécutez le programme en tapant ./hello
Si vous avez tout fait correctement, le message "Hello, world!" sur l'écran.


## Manipulation et test des nombres #

### Adressage #

### 1. Lecture #

Pour désigner la valeur de quelque chose, on utilise son nom, par exemple superEtiquette ou EAX. Pour désigner ce qu'il pointe (la zone mémoire qui porte son adresse), on le met entre crochets, par exemple [superEtiquette] ou [EAX]. Et pour charger, on utilise comme d'habitude mov.


Exemple 1 : db pour definir 1 octet
```
org 100h

mov  AL, [genial]   ; AL = 47h
mov  AH, [ordi]     ; AH = 2Eh
mov  EBX,genial
mov  CH, [EBX]       ; CH = 47h
ret

genial: db 47h
ordi:   db 2Eh
```

![ALT](adressageLecture1.png)

Exemple 2 : dw pour un mot (pour definir 2 octets)
```
org 100h

mov  AX, [etiquette]  ; AX = 0xE3F6
mov  EDX,etiquette
mov  BX, [EDX]        ; BX = 0xE3F6
ret

etiquette: dw 0E3F6h
```

![ALT](adressageLecture1.png)

![ALT](adressageLecture1.png)

### 2. Ecriture #

#### Incrémentation et décrémentation <br />
Ce sont deux opérations très basiques : l'instruction inc pour incrémenter (ajouter 1) et dec pour décrémenter (retrancher 1). Elle est compatible avec les registres mais également avec l'adressage.

#### Additions et soustractions <br />
On utilise les mots-clé add et sub et on renseigne bien sûr les opérandes. Le premier argument doit être un registre et le second argument soit un registre de même taille, soit une constante de valeur compatible.

#### Multiplications et divisions euclidiennes <br />

```
mul  r/m8        ; AX = AL*op1
mul  r/m16       ; DX:AX = AX*op1
mul  r, r/m/imm  ; op1 = op1*op2 (avec op1 et op2 le même nombre de bits : 8 ou 16)
mul  r, r/m, imm ; op1 = op2*op3 (avec op1, op2 et op3 le même nombre de bits : 8 ou 16)
div  r/m8        ; AX/op1 ==> AL = quotient ; AH = reste
div  r/m16       ; DX:AX/op1 ==> AX = quotient ; DX = reste
```

- r = registre. Par exemple, BH est un registre.
- m = donnée en mémoire. Par exemple, byte[adresse] est une donnée en mémoire.
- imm = valeur immédiate, c'est-à-dire une constante. Par exemple, 5 et 78 sont des valeurs immédiates.

![ALT](registrevirtuelDXAX.png)

### 3. Les drapeaux #

L'assembleur x86 contient des drapeaux (flags en anglais) qui nous donnent toutes sortes d'informations. Tout comme un vrai drapeau ou panneau n'a que deux côtés, un drapeau en assembleur n'a que deux valeurs possibles : 0 ou 1. Nous verrons plus tard qu'un drapeau est en fait un bit et que les drapeaux sont rangés dans le registre EFLAGS. <br />
Les drapeaux de statut :
- **CF : Carry Flag**
Permet essentiellement de stocker d'éventuelles retenues, qui peuvent se montrer sur des instructions donnant lieu à des débordements de capacité. En décimal, quand on fait une opération à la main, une retenue est comprise entre 0 et 9. En binaire, elle sera comprise entre 0 et 1 : un bit (ce qui est logique puisqu'une retenue est un chiffre donc ici un chiffre binaire).
- **AF : Auxiliary carry Flag**
Indique si la dernière opération a généré une retenue du bit 3 vers le bit 4.
- **OF : Overflow Flag**
Encore un drapeau sur le débordement de capacité suite à une opération.
- **ZF : Zero Flag**
Ce drapeau est modifié par certaines instructions pour indiquer si le résultat est égal à 0.
- **SF : Sign Flag**
À la suite de certaines instructions, est modifié en fonction du signe du résultat d'une opération signée.
- **PF : Parity Flag**
Détermine si le nombre de bits positionnés à un du résultat d'une opération est pair ou impair.

#### L'affectation des drapeaux <br />
En assembleur, ce sont des instructions qui modifient la valeur des drapeaux. C'est par exemple le cas de l'instruction de comparaison :
```
cmp  r, r/m/imm
```
Bien entendu, les deux arguments doivent faire le même nombre de bits. Soustrait VIRTUELLEMENT op2 à op1 (virtuellement car le résultat n'est stocké nulle part). Les drapeaux sont affectés en conséquence :
ZF : à 1 si le résultat est nul (donc si op1 = op2)
SF : à 1 si le résultat de l'opération signée est négatif (donc si op1 < op2 en signé)
CF : à 1 si le résultat non signé est négatif (donc si op1 < op2 en non signé)

**Exemple :**
```
mov  BX, 15
cmp  BX, 16
; ZF = 0
; SF = 1
; CF = 1
```

#### L'utilisation des drapeaux <br />
En assembleur, chaque drapeau possède des mnémoniques qui permettent de constituer des instructions.

![ALT](tableauDrapeaux.png)

Pour faire un saut conditionnel, on utilise le mot-clé commençant par la lettre "J" suivie du mnémonique. Et on renseigne en argument l'endroit où sauter (l'étiquette d'arrivée). Cela donne une instruction demandant de poursuivre l'exécution ailleurs si la condition est vraie.
Exemple :

```
org 100h

     mov  ah,0x09  ; on se prépare à afficher du texte sous DOS
     mov  bx,10
     mov  cx,15
     cmp  bx,cx
     ; instruction manquante

PlusGrandOuEgal:
     mov  dx,superieur
     int  0x21
     ret
PlusPetit:
     mov  dx,inferieur
     int  0x21
     ret

superieur: db "BX est plus grand que CX", 10, 13, "$"
inferieur: db "BX est plus petit que CX", 10, 13, "$"
```

![ALT](analyseCode.png)

Il est possible de faire un saut qui ne dépend pas d'une condition. Le saut sera effectué de toutes façons. On utilise le mot-clé "jmp" (comme "jump" = saut) suivi d'un nom d'étiquette.


### 4. Les instructions logiques

#### AND <br />
```
and  r/m, r/m/imm
```
Les bits sont analysés deux à deux : un pour op1, un pour op2. À chaque fois, un bit est renvoyé : 1 si les deux étaient à 1, sinon 0.

![ALT](tableVeriteAND.png)

Exemple :
```
mov  ah,11001110b
mov  al,10110010b
and  al,ah
; al = 10000010b
```

#### OR <br />
```
or   r/m, r/m/imm
```
Le principe est le même que AND sauf que OR renvoie un 1 si au moins un des deux bits en entrée est à 1 :

![ALT](tableVeriteOR.png)

Exemple :
```
mov  ah,11001110b
mov  al,10110010b
or   al,ah
; al = 11111110b
```

#### XOR <br />
```
xor  r/m, r/m/imm
```
Cette fois, la sortie est à 1 si une et une seule des deux entrées est à 1 (autrement dit si les deux entrées ont une valeur différente) :

![ALT](tableVeriteXOR.png)

Exemple :
```
mov  ah,11001110b
mov  al,10110010b
xor  al,ah
; al = 01111100b
```

#### NOT <br />
```
not  r/m
```
Inverse tous les bits de op1. Le résultat est naturellement renvoyé dans op1.

![ALT](tableVeriteNOT.png)

Exemple :
```
mov  ah,11001110b
not  ah
; ah = 00110001b
```
