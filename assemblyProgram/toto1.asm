org 0x100

     mov  AL,'F' ; caractère à afficher AL = F
     xor  BH,BH  ; eh oui, il faut optimiser ! BH = 0 (page d'affichage)
     mov  CX,1   ; nombre de fois à afficher le caractère CX = 1

     mov  AH,0Eh
     int  10h

     ret