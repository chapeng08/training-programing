org 100h

     mov  ah,0x09  ; on se prépare à afficher du texte sous DOS
     mov  bx,10 ; bx = 10
     mov  cx,15 ; cx = 15
     cmp  bx,cx ; cx-bx = 15-10 = 5
                ; ZF = 0 SF = 1 CF = 1 donc op1 < op2 => bx < cx => bx-cx < 0 donc retenue negative => C, B et NAE  
     jc   PlusPetit

PlusGrandOuEgal:
     mov  dx,superieur
     int  0x21
     ret

PlusPetit:
     mov  dx,inferieur ; resultat de l'etiquette inferieur stockee dans dx
     int  0x21 ; appel a la routine DOS d'affichage
     ret ; fin du programme

superieur: db "BX est plus grand que CX", 10, 13, "$"
inferieur: db "BX est plus petit que CX", 10, 13, "$"